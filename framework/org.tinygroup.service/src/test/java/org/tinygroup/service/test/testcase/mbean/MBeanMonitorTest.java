package org.tinygroup.service.test.testcase.mbean;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import junit.framework.TestCase;

import org.tinygroup.event.Parameter;
import org.tinygroup.service.PrintContextService;
import org.tinygroup.service.mbean.ServiceMonitor;
import org.tinygroup.service.registry.ServiceRegistry;
import org.tinygroup.service.registry.ServiceRegistryItem;
import org.tinygroup.service.registry.impl.ServiceRegistryImpl;

/**
 * 
 * @description：
 * @author: qiucn
 * @version: 2016年6月29日 上午10:27:56
 */
public class MBeanMonitorTest extends TestCase{
	
	public void testmbean() throws Exception {
		ServiceRegistry serviceRegistry = new ServiceRegistryImpl();
		serviceRegistry.clear();
		PrintContextService printContextService = new PrintContextService();
		ServiceRegistryItem item = new ServiceRegistryItem();
		item.setServiceId("aaaa");
		Parameter parameterDescriptor = new Parameter();
		parameterDescriptor.setArray(false);
		parameterDescriptor.setName("aa");
		parameterDescriptor.setType("java.lang.String");
		parameterDescriptor.setRequired(true);
		item.setService(printContextService);
		List<Parameter> parameterDescriptors = new ArrayList<Parameter>();
		parameterDescriptors.add(parameterDescriptor);
		item.setParameters(parameterDescriptors);
		item.setResults(parameterDescriptors);
		serviceRegistry.registerService(item);
		
		MBeanServer mb = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("TinyMBean:name=ServiceMonitor");
		ServiceMonitor sm = new ServiceMonitor();
		sm.setServiceRegistry(serviceRegistry);
        mb.registerMBean(sm, name);
        Integer o = (Integer) mb.getAttribute(name, "ServiceTotal");
        assertEquals(1, o.intValue());
        boolean o1 = (Boolean) mb.invoke(name, "isExistLocalService", new Object[]{"aaaa"}, new String[]{"java.lang.String"});   
        assertEquals(true, o1);
	}
}
