package org.tinygroup.cepcoregovernance.test.testcase.mbean;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import junit.framework.TestCase;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.cepcore.CEPCore;
import org.tinygroup.cepcoregovernance.container.ExecuteTimeInfo;
import org.tinygroup.cepcoregovernance.mbean.GovernanceMonitor;
import org.tinygroup.cepcoregovernance.test.testcase.LocalServiceTestCase;
import org.tinygroup.context.util.ContextFactory;
import org.tinygroup.event.Event;
import org.tinygroup.tinyrunner.Runner;



/**
 * 
 * @description：
 * @author: qiucn
 * @version: 2016年6月29日 上午10:27:56
 */
public class MBeanMonitorTest extends TestCase{
	
	private CEPCore core = null;

	public void setUp() {
		Runner.stop();
		Runner.init("application.xml", new ArrayList<String>());
		core = BeanContainerFactory.getBeanContainer(
				LocalServiceTestCase.class.getClassLoader()).getBean(
				CEPCore.CEP_CORE_BEAN);
	}
	
	public void testMBean() throws Exception {
		for(int i = 0 ; i < 1000 ; i ++ ){
			executeService("exceptionService");
			executeService("localService");
		}
		
		MBeanServer mb = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("TinyMBean:name=GovernanceMonitor");
		GovernanceMonitor gm = new GovernanceMonitor();
        mb.registerMBean(gm, name);
        Long o = (Long) mb.getAttribute(name, "ExceptionTotal");   
        assertEquals(1000, o.intValue());
        Long o1 = (Long) mb.getAttribute(name, "LocalTotalTimes");   
        assertEquals(2000, o1.intValue());
        Long o2 = (Long) mb.getAttribute(name, "LocalSuccessTimes");   
        assertEquals(1000, o2.intValue());
        Long o3 = (Long) mb.getAttribute(name, "LocalExceptionTimes");   
        assertEquals(1000, o3.intValue());
        ExecuteTimeInfo o4 = (ExecuteTimeInfo) mb.invoke(name, "getLocalServiceExecuteTimeInfo", new Object[]{"localService"}, new String[]{"java.lang.String"});   
        assertEquals(1000, o4.getTimes());
	}

	private void executeService(String serviceId ) {
		try{
			core.process(getEvent(serviceId));
		}catch (Exception e) {
		}
	}

	private Event getEvent(String serviceId) {
		return Event.createEvent(serviceId, ContextFactory.getContext());
	}
}
