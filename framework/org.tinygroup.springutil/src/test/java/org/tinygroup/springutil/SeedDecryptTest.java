package org.tinygroup.springutil;

import junit.framework.TestCase;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SeedDecryptTest extends TestCase {

	ClassPathXmlApplicationContext applicationContext;

	@Override
	protected void setUp() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(
				new String[] { "seed.beans.xml" });
	}

	@Override
	protected void tearDown() throws Exception {
		applicationContext.close();
	}

	public void testDecrypt() throws Exception {
		JdbcPojo jdbcPojo = (JdbcPojo) applicationContext.getBean("seedJdbcPojo");
		assertEquals("opensource", jdbcPojo.getPassword());
	}
	
}
