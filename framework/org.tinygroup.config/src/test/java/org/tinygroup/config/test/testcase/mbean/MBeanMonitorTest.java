package org.tinygroup.config.test.testcase.mbean;

import java.lang.management.ManagementFactory;
import java.util.Map;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import junit.framework.TestCase;

import org.tinygroup.config.mbean.ConfigMonitor;
import org.tinygroup.config.util.ConfigurationUtil;



/**
 * 
 * @description：
 * @author: qiucn
 * @version: 2016年6月29日 上午10:27:56
 */
public class MBeanMonitorTest extends TestCase{
	
	@SuppressWarnings("unchecked")
	public void testMBean() throws Exception {
		ConfigurationUtil.getConfigurationManager().getConfiguration().put("aa", "11");
		MBeanServer mb = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("TinyMBean:name=ConfigMonitor");
		ConfigMonitor cm = new ConfigMonitor();
        mb.registerMBean(cm, name);
        Map<String, String> map = (Map<String, String>) mb.getAttribute(name, "Configurations");   
        assertEquals(1, map.size());
        Object o1 = mb.invoke(name, "getConfigration", new Object[]{"aa"}, new String[]{"java.lang.String"});   
        assertEquals("11", String.valueOf(o1));
	}

}
