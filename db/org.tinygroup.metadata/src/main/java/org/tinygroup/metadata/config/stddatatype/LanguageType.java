/**
 *  Copyright (c) 1997-2013, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.tinygroup.metadata.config.stddatatype;

import java.util.List;

import org.tinygroup.metadata.config.BaseObject;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 数据库语言
 * 
 * @author yancheng11334
 * 
 */
@XStreamAlias("language-type")
public class LanguageType extends BaseObject {

	/**
	 * 方言类型 0：编程语言 1：数据库方言
	 * 
	 */
	@XStreamAsAttribute
	int type;

	@XStreamImplicit
	private List<LanguageField> languageFieldList;

	public List<LanguageField> getLanguageFieldList() {
		return languageFieldList;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	public void setLanguageFieldList(List<LanguageField> languageFieldList) {
		this.languageFieldList = languageFieldList;
	}
}
