package org.tinygroup.database.exception;

/**
 * Created by wangwy11342 on 2016/6/8.
 */
public class DatabaseErrorCode {
    public static final String ERROR_CODE_PREFIX = "0TE120055";

    //表格被重复添加
    public static final String TABLE__ADD_ALREADY_ERROR = ERROR_CODE_PREFIX + "001";

}
