package org.tinygroup.database.exception;

import org.tinygroup.context.Context;
import org.tinygroup.exception.BaseRuntimeException;

import java.util.Locale;

/**
 * Created by wangwy11342 on 2016/6/8.
 */
public class DatabaseRuntimeException extends BaseRuntimeException{

    private static final long serialVersionUID = -2096292363827109578L;

    public DatabaseRuntimeException() {
        super();
    }

    public DatabaseRuntimeException(String errorCode, Context context) {
        super(errorCode, context);
    }

    public DatabaseRuntimeException(String errorCode, Object... params){
        super(errorCode, params);
    }

    public DatabaseRuntimeException(String errorCode, String defaultErrorMsg,
                                    Context context, Locale locale) {
        super(errorCode, defaultErrorMsg, context, locale);
    }

    public DatabaseRuntimeException(String errorCode, String defaultErrorMsg,
                                             Locale locale, Object... params) {
        super(errorCode, defaultErrorMsg, locale, params);
    }

    public DatabaseRuntimeException(String errorCode, String defaultErrorMsg,
                                             Locale locale, Throwable throwable, Object... params) {
        super(errorCode, defaultErrorMsg, locale, throwable, params);
    }

    public DatabaseRuntimeException(String errorCode, String defaultErrorMsg,
                                             Throwable throwable, Object... params) {
        super(errorCode, defaultErrorMsg, throwable, params);
    }

    public DatabaseRuntimeException(String errorCode, Throwable throwable,
                                             Object... params) {
        super(errorCode, throwable, params);
    }

    public DatabaseRuntimeException(Throwable cause) {
        super(cause);
    }
}
