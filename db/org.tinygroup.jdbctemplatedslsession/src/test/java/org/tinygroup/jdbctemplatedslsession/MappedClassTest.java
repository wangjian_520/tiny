package org.tinygroup.jdbctemplatedslsession;

import java.beans.PropertyDescriptor;
import java.util.Map;

import junit.framework.TestCase;

public class MappedClassTest  extends TestCase{
	
	public void testMappedClass(){
		BaseMappedClass<User> mappedClass=new BaseMappedClass<MappedClassTest.User>(User.class);
		Map<String, PropertyDescriptor> mappedFields = mappedClass.getMappedFields();
		PropertyDescriptor id=mappedFields.get("id");
		assertNotNull(id);
		PropertyDescriptor nickName=mappedFields.get("nickname");
		assertNotNull(nickName);
		nickName=mappedFields.get("nick_name");
		assertNotNull(nickName);
		PropertyDescriptor nickName2=mappedFields.get("nickname2");
		assertNotNull(nickName2);
		nickName2=mappedFields.get("nick_name2");
		assertNotNull(nickName2);
		PropertyDescriptor iphoneQQ=mappedFields.get("iphoneqq");
		assertNotNull(iphoneQQ);
		iphoneQQ=mappedFields.get("iphone_q_q");
		assertNotNull(iphoneQQ);
	}
	
	class User{
		private int id;
		private String nickName;
		private String nickName2;
		private String iphoneQQ;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNickName() {
			return nickName;
		}
		public void setNickName(String nickName) {
			this.nickName = nickName;
		}
		public String getNickName2() {
			return nickName2;
		}
		public void setNickName2(String nickName2) {
			this.nickName2 = nickName2;
		}
		public String getIphoneQQ() {
			return iphoneQQ;
		}
		public void setIphoneQQ(String iphoneQQ) {
			this.iphoneQQ = iphoneQQ;
		}
		
	}
}
