package org.tinygroup.xmlsignaturefilter;

import org.tinygroup.weblayer.WebContext;
import org.tinygroup.weblayer.webcontext.AbstractWebContextWrapper;

public class XmlSignatureWebContextImpl extends AbstractWebContextWrapper{

	private boolean requestFinished;
	
	public XmlSignatureWebContextImpl(WebContext wrappedContext,boolean tag){
		super(wrappedContext);
		this.requestFinished = tag;
	}

	public boolean isRequestFinished() {
		return requestFinished;
	}
	
}
