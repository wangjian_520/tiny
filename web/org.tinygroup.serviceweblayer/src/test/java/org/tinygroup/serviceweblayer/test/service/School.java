package org.tinygroup.serviceweblayer.test.service;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class School implements Serializable{

	private String str;
	private List<String> listStr;
	private Grade grade;
	private List<Grade> gradeList;
	private Grade[] gradeArray;
	
	public Grade[] getGradeArray() {
		return gradeArray;
	}
	public void setGradeArray(Grade[] gradeArray) {
		this.gradeArray = gradeArray;
	}
	public List<Grade> getGradeList() {
		return gradeList;
	}
	public void setGradeList(List<Grade> gradeList) {
		this.gradeList = gradeList;
	}
	public Grade getGrade() {
		return grade;
	}
	public void setGrade(Grade grade) {
		this.grade = grade;
	}
	public List<String> getListStr() {
		return listStr;
	}
	public void setListStr(List<String> listStr) {
		this.listStr = listStr;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	
}
