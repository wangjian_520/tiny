package org.tinygroup.serviceweblayer.test.service;

import java.util.List;

public class ServiceForTest {
	public int basicServiceInt(int i) {
		return i;
	}
	public char basicServiceChar(char i) {
		return i;
	}
	public char[] basicServiceCharArray(char[] i) {
		return i;
	}
	public Character basicServiceCharacter(Character i) {
		return i;
	}
	public Character[] basicServiceCharacterArray(Character[] i) {
		return i;
	}
	public List<Character> basicServiceCharacterList(List<Character> i) {
		return i;
	}

	public int[] basicServiceIntArray(int[] i, String a) {
		return i;
	}

	public boolean basicServiceBoolean(boolean i, String a) {
		return i;
	}

	public boolean[] basicServiceBooleanArray(boolean[] i, String a) {
		return i;
	}

	public Integer basicServiceInteger(Integer i, String a) {
		return i;
	}

	public Integer[] basicServiceIntegerArray(Integer[] i, String a) {
		return i;
	}

	public List<Integer> basicServiceIntegerList(List<Integer> i, String a) {
		return i;
	}

	public Boolean basicWrapperServiceBoolean(Boolean i, String a) {
		return i;
	}

	public List<Boolean> basicWrapperServiceBooleanList(List<Boolean> i,
			String a) {
		return i;
	}

	public Grade objectServiceGrade(Grade grade, String a) {
		return grade;
	}

	public List<Grade> objectServiceGradeList(List<Grade> grade, String a) {
		return grade;
	}

	public Grade[] objectServiceGradeArray(Grade[] grade, String a) {
		return grade;
	}

	public byte basicServiceByte(byte b){
		return b;
	}
	public byte[] basicServiceByteArray(byte[] bs){
		return bs;
	}
	public Byte basicServiceWrapperByte(Byte b){
		return b;
	}
	public Byte[] basicServiceWrapperByteArray(Byte[] bs){
		return bs;
	}
	public List<Byte> basicServiceWrapperByteList(List<Byte> bs){
		return bs;
	}
	
	public float basicServiceFloat(float f){
		return f;
	}
	public float[] basicServiceFloatArray(float[] f){
		return f;
	}
	public Float basicServiceWrapperFloat(Float f){
		return f;
	}
	public Float[] basicServiceWrapperFloatArray(Float[] f){
		return f;
	}
	public List<Float> basicServiceWrapperFloatList(List<Float> f){
		return f;
	}
	
	public School objectServiceSchool(School school) {
		return school;
	}

	public List<School> objectServiceSchoolList(List<School> school) {
		return school;
	}

	public School[] objectServiceSchoolArray(School[] school) {
		return school;
	}
}
