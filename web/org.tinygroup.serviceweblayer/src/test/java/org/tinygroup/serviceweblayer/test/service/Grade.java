package org.tinygroup.serviceweblayer.test.service;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Grade implements Serializable{
	String name;
	int age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
}
