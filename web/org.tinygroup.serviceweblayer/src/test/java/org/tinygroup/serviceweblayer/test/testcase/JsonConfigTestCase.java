package org.tinygroup.serviceweblayer.test.testcase;

import junit.framework.TestCase;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.serviceweblayer.processor.JsonServiceHeadConfigTinyProcessor;
import org.tinygroup.tinyrunner.Runner;

public class JsonConfigTestCase extends TestCase {

	public void testJson() {
		Runner.init(null, null);
		JsonServiceHeadConfigTinyProcessor processor = BeanContainerFactory
				.getBeanContainer(JsonConfigTestCase.class.getClassLoader()).getBean(
						JsonServiceHeadConfigTinyProcessor.BEAN_NAME);
		process(processor,"{\"head\":{\"functionId\":\"basicServiceChar\"},\"body\":{\"i\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceCharArray\"},\"body\":{\"i\":[\"a\",\"b\"]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceCharacter\"},\"body\":{\"i\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceCharacterArray\"},\"body\":{\"i\":[\"a\",\"b\"]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceCharacterList\"},\"body\":{\"i\":[\"a\",\"b\"]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceInt\"},\"body\":{\"i\":1}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceIntArray\"},\"body\":{\"i\":[1,2,3,4],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceBoolean\"},\"body\":{\"i\":true,\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceBooleanArray\"},\"body\":{\"i\":[true,false,true,false],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceInteger\"},\"body\":{\"i\":1,\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceIntegerArray\"},\"body\":{\"i\":[1,2,3,4],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceIntegerList\"},\"body\":{\"i\":[1,2,3,4],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicWrapperServiceBoolean\"},\"body\":{\"i\":true,\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicWrapperServiceBooleanList\"},\"body\":{\"i\":[true,false,true,false],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"objectServiceGrade\"},\"body\":{\"grade\":{\"name\":\"a\",\"age\":1},\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"objectServiceGradeList\"},\"body\":{\"grade\":[{\"name\":\"a\",\"age\":1},{\"name\":\"b\",\"age\":2},{\"name\":\"c\",\"age\":3}],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"objectServiceGradeArray\"},\"body\":{\"grade\":[{\"name\":\"a\",\"age\":1},{\"name\":\"b\",\"age\":2},{\"name\":\"c\",\"age\":3}],\"a\":\"a\"}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceByte\"},\"body\":{\"b\":85}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceByteArray\"},\"body\":{\"bs\":[85,6]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceWrapperByte\"},\"body\":{\"b\":85}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceWrapperByteArray\"},\"body\":{\"bs\":[85,6]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceWrapperByteList\"},\"body\":{\"bs\":[85,6]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceFloat\"},\"body\":{\"f\":1.2}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceFloatArray\"},\"body\":{\"f\":[1.2,1.3]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceWrapperFloat\"},\"body\":{\"f\":1.2}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceWrapperFloatArray\"},\"body\":{\"f\":[1.2,1.3]}}");
		process(processor,"{\"head\":{\"functionId\":\"basicServiceWrapperFloatList\"},\"body\":{\"f\":[1.2,1.3]}}");
		process(processor,"{\"head\":{\"functionId\":\"objectServiceSchool\"},\"body\":{\"school\":{\"str\":\"abc\",\"listStr\":[{\"str\":\"a\",\"str\":\"b\"}],\"grade\":{\"name\":\"a\",\"age\":1},\"gradeList\":[{\"name\":\"b\",\"age\":2},{\"name\":\"c\",\"age\":3}],\"gradeArray\":[{\"name\":\"d\",\"age\":4},{\"name\":\"e\",\"age\":5}]}}}");
		process(processor,"{\"head\":{\"functionId\":\"objectServiceSchoolList\"},\"body\":{\"school\":[{\"str\":\"abc\",\"listStr\":[{\"str\":\"b\"}],\"grade\":{\"name\":\"a\",\"age\": 1},\"gradeList\": [{\"name\": \"b\",\"age\": 2},{\"name\": \"c\",\"age\": 3}],\"gradeArray\": [{\"name\": \"d\",\"age\": 4},{\"name\": \"e\",\"age\": 5}]},{\"str\": \"abc\",\"listStr\": [{\"str\": \"b\"}],\"grade\": {\"name\": \"a\",\"age\": 1},\"gradeList\": [{\"name\": \"b\",\"age\": 2},{\"name\": \"c\",\"age\": 3}],\"gradeArray\": [{\"name\": \"d\",\"age\": 4},{\"name\": \"e\",\"age\": 5}]}]}}");
		process(processor,"{\"head\":{\"functionId\":\"objectServiceSchoolArray\"},\"body\":{\"school\":[{\"str\":\"abc\",\"listStr\":[{\"str\":\"b\"}],\"grade\":{\"name\":\"a\",\"age\": 1},\"gradeList\": [{\"name\": \"b\",\"age\": 2},{\"name\": \"c\",\"age\": 3}],\"gradeArray\": [{\"name\": \"d\",\"age\": 4},{\"name\": \"e\",\"age\": 5}]},{\"str\": \"abc\",\"listStr\": [{\"str\": \"b\"}],\"grade\": {\"name\": \"a\",\"age\": 1},\"gradeList\": [{\"name\": \"b\",\"age\": 2},{\"name\": \"c\",\"age\": 3}],\"gradeArray\": [{\"name\": \"d\",\"age\": 4},{\"name\": \"e\",\"age\": 5}]}]}}");
		assertTrue(true);
	}

	private void process(JsonServiceHeadConfigTinyProcessor processor, String json) {
		System.out.println("request:");
		System.out.println("		"+json);
		String result = processor.process(json);
		System.out.println("result:");
		System.out.println("		"+result);
	}
}
