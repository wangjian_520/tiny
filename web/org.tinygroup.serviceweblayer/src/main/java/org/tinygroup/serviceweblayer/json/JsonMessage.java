package org.tinygroup.serviceweblayer.json;

import java.util.Map;

public abstract class JsonMessage {
	public final static String SERVICE_ID = "serviceid";
	public final static String ERROR_INFO = "ErrorInfo";
	public final static String ERROR_CODE = "ErrorCode";
	
	private Map<String, String> head;
	
	
	public Map<String, String> getHead() {
		return head;
	}
	public void setHead(Map<String, String> head) {
		this.head = head;
	}
	
}
