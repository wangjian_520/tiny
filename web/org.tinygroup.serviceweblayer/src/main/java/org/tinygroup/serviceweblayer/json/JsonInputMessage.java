package org.tinygroup.serviceweblayer.json;

public class JsonInputMessage extends JsonMessage {
	private String body;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
