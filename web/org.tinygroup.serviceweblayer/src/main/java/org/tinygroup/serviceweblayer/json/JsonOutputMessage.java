package org.tinygroup.serviceweblayer.json;

import java.util.HashMap;
import java.util.Map;

public class JsonOutputMessage extends JsonMessage {
	private Map<String,Object> body = new HashMap<String, Object>();


	public Map<String, Object> getBody() {
		return body;
	}

	public void setBody(Map<String, Object> body) {
		this.body = body;
	}}
