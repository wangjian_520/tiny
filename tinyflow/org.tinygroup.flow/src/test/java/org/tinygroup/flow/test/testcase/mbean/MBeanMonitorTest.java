package org.tinygroup.flow.test.testcase.mbean;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.tinygroup.flow.component.AbstractFlowComponent;
import org.tinygroup.flow.mbean.FlowMonitor;

/**
 * 
 * @description：
 * @author: qiucn
 * @version: 2016年6月29日 上午10:27:56
 */
public class MBeanMonitorTest extends AbstractFlowComponent {

	/**
	 * 
	 * @description：逻辑流程监控
	 * @author: qiucn
	 * @version: 2016年6月30日下午4:27:54
	 */
	public void testFlowMBean() throws Exception {

		MBeanServer mb = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("TinyMBean:name=FlowMonitor");
		FlowMonitor sm = new FlowMonitor();
		sm.setFlowExecutor(flowExecutor);
		mb.registerMBean(sm, name);
		Object o = mb.invoke(name, "isExistFlowService",
				new Object[] { "flowExceptionTest" },
				new String[] { "java.lang.String" });
		assertEquals(true, o);
		Object o1 = mb.invoke(name, "isExistComponent",
				new Object[] { "exceptionSwitchComponent" },
				new String[] { "java.lang.String" });
		assertEquals(true, o1);
		Integer o2 = (Integer) mb.getAttribute(name, "FlowServiceTotal");
		assertEquals(flowExecutor.getFlowIdMap().size(), o2.intValue());
		Integer o3 = (Integer) mb.getAttribute(name, "ComponentTotal");
		assertEquals(flowExecutor.getComponentDefines().size(), o3.intValue());
	}

	/**
	 * 
	 * @description：页面流程监控
	 * @author: qiucn
	 * @version: 2016年6月30日下午4:28:02
	 */
	public void testPageflowMBean() throws Exception {

		MBeanServer mb = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("TinyMBean:name=PageFlowMonitor");
		FlowMonitor sm = new FlowMonitor();
		sm.setFlowExecutor(pageFlowExecutor);
		mb.registerMBean(sm, name);
		Object o = mb.invoke(name, "isExistFlowService",
				new Object[] { "pageflowreleaseFlow" },
				new String[] { "java.lang.String" });
		assertEquals(true, o);
		Object o1 = mb.invoke(name, "isExistComponent",
				new Object[] { "sumComponentPageFlow" },
				new String[] { "java.lang.String" });
		assertEquals(true, o1);
		Integer o2 = (Integer) mb.getAttribute(name, "FlowServiceTotal");
		assertEquals(pageFlowExecutor.getFlowIdMap().size(), o2.intValue());
		Integer o3 = (Integer) mb.getAttribute(name, "ComponentTotal");
		assertEquals(pageFlowExecutor.getComponentDefines().size(), o3.intValue());
	}
}
