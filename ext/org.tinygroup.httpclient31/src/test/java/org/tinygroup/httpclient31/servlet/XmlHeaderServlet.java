package org.tinygroup.httpclient31.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试对xml格式的header支持
 * @author yancheng11334
 *
 */
public class XmlHeaderServlet extends AbstractMockServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9151906446423187435L;

	protected void dealService(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String xml = request.getHeader("tiny-xmlsignature");
		response.getWriter().write(xml);
	}

}
