/**
 * 
 */
package org.tinygroup.remoteconfig.config;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

/**
 * @author yanwj06282
 * 
 */
public class ConfigValue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3225816156526808185L;
	String key;
	String value;
	String title;
	String desc;

	public ConfigValue() {
	}
	
	public ConfigValue(String value) {
		super();
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return getValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ConfigValue) {
			if (StringUtils.equals(((ConfigValue) obj).getKey(), getKey())
					&& StringUtils.equals(((ConfigValue) obj).getValue(), getValue())
					&& StringUtils.equals(((ConfigValue) obj).getTitle(), getTitle())
					&& StringUtils.equals(((ConfigValue) obj).getDesc(), getDesc())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return StringUtils.defaultString(getKey()).hashCode() & StringUtils.defaultString(getValue()).hashCode() & StringUtils.defaultString(getValue()).hashCode() & StringUtils.defaultString(getDesc()).hashCode();
	}
	
}
