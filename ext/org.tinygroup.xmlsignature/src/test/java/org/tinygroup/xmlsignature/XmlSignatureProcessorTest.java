package org.tinygroup.xmlsignature;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.commons.tools.FileUtil;
import org.tinygroup.tinyrunner.Runner;

import junit.framework.TestCase;

public class XmlSignatureProcessorTest extends TestCase{

	private XmlSignatureProcessor envelopedProcessor = null;
	
	protected void setUp() throws Exception {
		Runner.initDirect("application.xml", null);
		envelopedProcessor = BeanContainerFactory.getBeanContainer(getClass().getClassLoader()).getBean("envelopedXmlSignatureProcessor");
	}
	
	public void testEnvelopedProcessor() throws Exception{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try{
			//生成签名接口
			envelopedProcessor.createXmlSignature("SERVER007", "src/test/resources/simple.xml", output);
			String signResult = new String(output.toByteArray(),"utf-8");
			String content = FileUtil.readFileContent(new File("src/test/resources/enveloped_ok.xml"), "utf-8");
			assertEquals(content, signResult);
			
			//验证签名接口
			assertEquals(true, envelopedProcessor.validateXmlSignature("SERVER007", "src/test/resources/enveloped_ok.xml"));
			assertEquals(false, envelopedProcessor.validateXmlSignature("SERVER007", "src/test/resources/enveloped_changed.xml"));
		}finally{
			output.close();
		}
	}
}
