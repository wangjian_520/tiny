package org.tinygroup.xmlsignature;

import org.tinygroup.tinyrunner.Runner;
import org.tinygroup.xmlparser.XmlDocument;
import org.tinygroup.xmlparser.node.XmlNode;
import org.tinygroup.xmlparser.parser.XmlStringParser;
import org.tinygroup.xmlsignature.impl.StringXmlSignatureHelper;

import junit.framework.TestCase;

public class StringXmlSignatureHelperTest extends TestCase{

	protected void setUp() throws Exception {
		Runner.initDirect("application.xml", null);
	}
	
	public void testString(){
		StringXmlSignatureHelper helper = new StringXmlSignatureHelper();
		assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Access><UserId>GSYH001</UserId></Access>", helper.getTemplateXml(null));
		assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Access><UserId>SERVER007</UserId></Access>", helper.getTemplateXml("SERVER007"));
		
		//获得userId
		XmlDocument doc = new XmlStringParser().parse("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Access><UserId>GSYH001</UserId></Access>");
		XmlNode node = doc.getRoot();
		assertEquals("GSYH001",node.getSubNode("UserId").getContent());
	}
}
