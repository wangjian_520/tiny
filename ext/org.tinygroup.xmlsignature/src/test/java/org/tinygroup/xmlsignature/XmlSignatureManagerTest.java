package org.tinygroup.xmlsignature;

import java.security.KeyPair;

import junit.framework.TestCase;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.tinyrunner.Runner;
import org.tinygroup.xmlsignature.config.XmlSignatureConfig;

/**
 * 测试XML数字签名的加载
 * @author yancheng11334
 *
 */
public class XmlSignatureManagerTest extends TestCase{

	private XmlSignatureManager manager;
	
	protected void setUp() throws Exception {
		Runner.initDirect("application.xml", null);
		manager = BeanContainerFactory.getBeanContainer(getClass().getClassLoader()).getBean(XmlSignatureManager.DEFAULT_BAEN_NAME);
	}
	
	public void testBase(){
		XmlSignatureConfig config = null;
		config = manager.getXmlSignatureConfig("GSYH001");
		assertEquals("/opt/public.jks", config.getPublicKeyPath());
		assertEquals("/opt/private.jks", config.getPrivateKeyPath());
		
		config = manager.getXmlSignatureConfig("HZGF");
		assertEquals("/opt/1.keystore", config.getPublicKeyPath());
		assertEquals("/opt/2.keystore", config.getPrivateKeyPath());
	}
	
	public void testKeyPair(){
		KeyPair keyPair = manager.getKeyPair("SERVER007");
		assertNotNull(keyPair);
		assertEquals("RSA",keyPair.getPublic().getAlgorithm());
		assertEquals("RSA",keyPair.getPrivate().getAlgorithm());
	}
}
