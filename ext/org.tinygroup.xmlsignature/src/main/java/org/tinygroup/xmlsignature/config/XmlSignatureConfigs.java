package org.tinygroup.xmlsignature.config;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * XML数字签名配置组
 * @author yancheng11334
 *
 */
@XStreamAlias("xml-signatures")
public class XmlSignatureConfigs {

	@XStreamImplicit
	private List<XmlSignatureConfig>  xmlSignatureConfigList;

	public List<XmlSignatureConfig> getXmlSignatureConfigList() {
		return xmlSignatureConfigList;
	}

	public void setXmlSignatureConfigList(
			List<XmlSignatureConfig> xmlSignatureConfigList) {
		this.xmlSignatureConfigList = xmlSignatureConfigList;
	}
}
