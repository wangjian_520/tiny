package org.tinygroup.xmlsignature;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.crypto.dsig.XMLSignatureException;

/**
 * XML数字签名处理器
 * @author yancheng11334
 *
 */
public interface XmlSignatureProcessor {
    
	/**
	 * 生成XML数字签名
	 * @param userId
	 * @param input
	 * @param output
	 * @throws XMLSignatureException
	 */
	void  createXmlSignature(String userId,InputStream input,OutputStream output) throws XMLSignatureException;
	
	/**
	 * 生成XML数字签名
	 * @param userId
	 * @param sourceXmlPath
	 * @param output
	 * @throws XMLSignatureException
	 */
	void  createXmlSignature(String userId,String sourceXmlPath,OutputStream output) throws XMLSignatureException;
	
	 /**
	  * 验证XML数字签名
	  * @param userId
	  * @param input
	  * @return
	  * @throws XMLSignatureException
	  */
	 boolean validateXmlSignature(String userId,InputStream input) throws XMLSignatureException;
	
	/**
	 * 验证XML数字签名
	 * @param userId
	 * @param signedXmlPath
	 * @return
	 * @throws XMLSignatureException
	 */
	boolean validateXmlSignature(String userId,String signedXmlPath) throws XMLSignatureException;
}
