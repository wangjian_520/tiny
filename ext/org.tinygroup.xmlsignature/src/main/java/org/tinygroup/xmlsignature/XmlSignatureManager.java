package org.tinygroup.xmlsignature;

import java.security.KeyPair;
import java.util.List;

import org.tinygroup.xmlsignature.config.XmlSignatureConfig;
import org.tinygroup.xmlsignature.config.XmlSignatureConfigs;

/**
 * XML数字签名管理器
 * @author yancheng11334
 *
 */
public interface XmlSignatureManager {

	public static final String DEFAULT_BAEN_NAME = "xmlSignatureManager" ;
	
	/**
	 * 添加XML数字签名配置项
	 * @param config
	 */
	void addXmlSignatureConfig(XmlSignatureConfig config);
	
	/**
	 * 删除XML数字签名配置项
	 * @param userId
	 */
	void removeXmlSignatureConfig(String userId);
	
	/**
	 * 读取XML数字签名配置项
	 * @param userId
	 * @return
	 */
	XmlSignatureConfig getXmlSignatureConfig(String userId);
	
	/**
	 * 获得XML数字签名配置项列表
	 * @return
	 */
	List<XmlSignatureConfig>  getXmlSignatureConfigList();
	
	/**
	 * 根据配置项获得公钥私钥对
	 * @param userId
	 * @return
	 */
	KeyPair getKeyPair(String userId);
	
	/**
	 * 添加XML数字签名配置组
	 * @param configs
	 */
	void addXmlSignatureConfigs(XmlSignatureConfigs configs);
	
	/**
	 * 删除XML数字签名配置组
	 * @param configs
	 */
	void removeXmlSignatureConfigs(XmlSignatureConfigs configs);
}
