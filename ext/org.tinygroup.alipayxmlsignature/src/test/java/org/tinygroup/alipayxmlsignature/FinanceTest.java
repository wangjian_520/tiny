package org.tinygroup.alipayxmlsignature;

import java.io.ByteArrayOutputStream;
import java.io.File;

import junit.framework.TestCase;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.commons.tools.FileUtil;
import org.tinygroup.tinyrunner.Runner;
import org.tinygroup.xmlsignature.XmlSignatureProcessor;

public class FinanceTest extends TestCase{

	private XmlSignatureProcessor financeProcessor = null;
	protected void setUp() throws Exception {
		Runner.initDirect("application.xml", null);
		financeProcessor = BeanContainerFactory.getBeanContainer(getClass().getClassLoader()).getBean("financeXmlSignatureProcessor");
	}
	
	/**
	 * 验证Finance格式报文<br>
	 * finance_s1.xml:原始报文，没有经过数字证书签名<br>
	 * finance_g1.xml:正确的经过数字证书签名报文<br>
	 * finance_g11.xml:不正确的经过数字证书签名报文(修改了部分字段)<br>
	 * @throws Exception
	 */
	public void testFinance() throws Exception{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try{
			//生成签名接口
			financeProcessor.createXmlSignature("SERVER007", "src/test/resources/finance_s1.xml", output);
			String signResult = new String(output.toByteArray(),"utf-8");
			String content = FileUtil.readFileContent(new File("src/test/resources/finance_g1.xml"), "utf-8");
			assertEquals(content, signResult);
			
			//验证签名接口
			assertEquals(true, financeProcessor.validateXmlSignature("SERVER007", "src/test/resources/finance_g1.xml"));
			assertEquals(false, financeProcessor.validateXmlSignature("SERVER007", "src/test/resources/finance_g11.xml"));
		}finally{
			output.close();
		}
	}
}
